﻿using System;

namespace лаб3_номер_3
{
    class Program
    {
        static void Main(string[] args)
        {
            //Пользователь задаёт число словом «один», «два», «три», «четыре», «пять» (присваивает его объявленной переменной).
            //Программа должна вывести значение числом на экран.

            //Команда ,которая просит полозвателя написать число
            Console.WriteLine("Введите число словом:");

            //Команда ,которая считывает тип данных string
            string Number = Console.ReadLine().ToLower();

            //Начало конструкции switch ,которая сравнивает несколько условий
            switch (Number)
            {
                case "один":
                    Console.WriteLine("1");
                    break;
                case "два":
                    Console.WriteLine("2");
                    break;
                case "три":
                    Console.WriteLine("3");
                    break;
                case "четыре":
                    Console.WriteLine("4");
                    break;
                case "пять":
                    Console.WriteLine("5");
                    break;

                default:
                    Console.WriteLine("Такая цифра не подходит");
                    break;
            }
        }
    }
}
