﻿using System;

namespace лаб3_номер2
{
    class Program
    {
        static void Main(string[] args)
        {
            //f(x, y, z) = z&y&!x|y = false

            //Объявляется функцмя которая будет проверять булевые переменны x,y,z
            static void Сheck(bool x, bool y, bool z)
            {   
                //Блок кода ,в котором условие проверки
                bool f = z & y & !x | y;
                if (f == false)
                {
                    Console.WriteLine("x - {0}, y - {1}, z - {2}", x, y, z);
                }
            }
            //Возможные варианты для проверки
            Сheck(false, false, false);
            Сheck(false, true, false);
            Сheck(false, false, true);
            Сheck(false, true, true);
            Сheck(true, true, true);
            Сheck(true, true, false);
            Сheck(true, false, false);
            Сheck(true, false, true);
            
            Console.ReadKey();
        }
    }
}
