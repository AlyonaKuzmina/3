﻿using System;

namespace лаб3_номер_1
{
    class Program
    {
        static void Main(string[] args)
        {
            //Команда ,которая говорит ползователю ввести значение переменной x
            Console.WriteLine("Введите значение переменной x,где x > -2.5");
            string xstr = Console.ReadLine();

            //Команда ,которая конвертирует строковы тип в тип double
            double x = Convert.ToDouble(xstr);

            //Объявляется условие 
            if (x > -2.5)
            {
                double y = 3 * x - 2;
                Console.WriteLine("Значение переменной y = " + y);
            }
            //Используется второе условие , если первое условие не подошло
            else if (x >= 0 && x <= -2.5) 
            {
                double y = 3 + x;
                Console.WriteLine("Значение переменной y = " + y);
            }
            //Используется третье условие ,если предыдущие не подошли
            else
            {
                double y = 3 * x;
                Console.WriteLine("Значение переменной y = " + y);
            }
        }
    }
}
